let objeto = {
    receitas: [100, 200, 50],
    despesas: [50, 200]
}

function retornoSaldo(obj) {
    let receitas = obj.receitas
    let despesas = obj.despesas
    
    const totalReceitas = receitas.reduce((soma, elemento) => {
        return soma + elemento
    })
    
    const totalDespesas = despesas.reduce((soma, elemento) => {
        return soma + elemento
    })
    
    const total = totalReceitas - totalDespesas 
    
    if (total < 0) {
        console.log("Seu saldo está negativo. Saldo:",total)
    }
    
    if (total > 0) {
        console.log("Seu saldo está positivo. Saldo:", total)
    }
}
retornoSaldo(objeto)



